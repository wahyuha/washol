FROM python:3
ENV PYTHONUNBUFFERED 1

ADD mm /var/mm
WORKDIR /var/mm
RUN pip install -r requirement.txt

EXPOSE 8000
CMD ["python3", "manage.py", "runserver", "0.0.0.0:8000"]